#include "../include/processor_node.hpp"
namespace rm_auto_aim
{
    ProcessorNode::ProcessorNode(const rclcpp::NodeOptions &options)
        : Node("armor_processor_node", options)
    {
    }
    void ProcessorNode::targetMsgCallback(const auto_aim_interfaces::msg::Autoaim &target_info)
    {
        auto_aim_interfaces::msg::Autoaim target = std::move(target_info);
    }
    void ProcessorNode::imageCallback(const sensor_msgs::msg::Image::ConstSharedPtr &img_msg)
    {
        cv::Mat src_ = cv_bridge::toCvShare(img_msg, "rgb8")->image;
    }
    ProcessorNode::~ProcessorNode()
    {
    }
} // namespace rm_auto_aim
using namespace std;
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<rm_auto_aim::ProcessorNode>());
    rclcpp::shutdown();
    return 0;
}
#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(rm_auto_aim::ProcessorNode)