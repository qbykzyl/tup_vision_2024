#include <iostream>

// ros相关
#include <rclcpp/rclcpp.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/msg/image.hpp>
#include <sensor_msgs/msg/camera_info.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <image_transport/publisher.hpp>
#include <image_transport/image_transport.hpp>
#include <image_transport/subscriber_filter.hpp>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <ament_index_cpp/get_package_share_directory.hpp>
#include <visualization_msgs/msg/marker.hpp>
#include <visualization_msgs/msg/marker_array.hpp>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
// std命名空间下
#include <mutex>
#include <atomic>
#include <thread>
// 自定义头文件
#include "auto_aim_interfaces/msg/autoaim.hpp"
#include "auto_aim_interfaces/msg/gimbal.hpp"
using namespace message_filters;
using namespace ament_index_cpp;
namespace rm_auto_aim
{
    class ProcessorNode : public rclcpp::Node
    {
        rclcpp::Subscription<auto_aim_interfaces::msg::Autoaim>::SharedPtr armor_msg_sub_;
        rclcpp::Publisher<auto_aim_interfaces::msg::Gimbal>::SharedPtr gimbal_msg_pub_;

    public:
        explicit ProcessorNode(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());
        ~ProcessorNode();
        void targetMsgCallback(const auto_aim_interfaces::msg::Autoaim &armor_msg);
        void imageCallback(const sensor_msgs::msg::Image::ConstSharedPtr &img_msg);
    };
}
