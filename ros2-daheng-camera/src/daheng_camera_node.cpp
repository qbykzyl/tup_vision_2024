#include <DxImageProc.h>
#include <GxIAPI.h>

// ROS
#include <camera_info_manager/camera_info_manager.hpp>
#include <image_transport/image_transport.hpp>
#include <rclcpp/logging.hpp>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/camera_info.hpp>
#include <sensor_msgs/msg/image.hpp>

// OpenCV
#include <opencv2/opencv.hpp>

// C++ system
#include <memory>
#include <string>
#include <thread>
#include <vector>
namespace daheng_camera
{
    class DaHengCameraNode : public rclcpp::Node
    {
    private:
        GX_STATUS status;
        GX_DEV_HANDLE hDevice;
        PGX_FRAME_BUFFER pFrameBuffer;
        // int64_t g_nPayloadSize = 0; ///< Payload size
        sensor_msgs::msg::Image image_msg_;
        image_transport::CameraPublisher camera_pub_;
        std::string camera_name_;
        std::unique_ptr<camera_info_manager::CameraInfoManager> camera_info_manager_;
        sensor_msgs::msg::CameraInfo camera_info_msg_;
        int point = 0;
        int fail_conut_ = 0;
        void checkPoint(GX_STATUS &state)
        {
            point++;
            if (state != GX_STATUS_SUCCESS)
                RCLCPP_ERROR(this->get_logger(), "check point%d failed ,state= %d", point, state);
            else if (point == 6)
                RCLCPP_INFO(this->get_logger(), "init camera succeed");
            else if (point == 9)
                RCLCPP_INFO(this->get_logger(), "begin read video");
        }

    public:
        ~DaHengCameraNode() override
        {
            RCLCPP_INFO(this->get_logger(), "Camera node destroyed!");
        }
        explicit DaHengCameraNode(const rclcpp::NodeOptions &options) : Node("daheng_camera", options)
        {
            RCLCPP_INFO(this->get_logger(), "Starting DaHengCameraNode!");
            uint32_t nDeviceNum = 0;
            GX_OPEN_PARAM stOpenParam;
            hDevice = NULL;
            status = GXInitLib();                                                                    // 若返回0值则代表成功
            checkPoint(status);                                                                      // check point 1
            status = GXUpdateAllDeviceList(&nDeviceNum, 1000);                                       // 枚举设备,设备数，等待时间
            checkPoint(status);                                                                      // check point 2
            stOpenParam.accessMode = GX_ACCESS_EXCLUSIVE;                                            // 独占模式打开相机
            stOpenParam.openMode = GX_OPEN_INDEX;                                                    // 索引号打开模式
            stOpenParam.pszContent = "1";                                                            // 索引号1
            status = GXOpenDevice(&stOpenParam, &hDevice);                                           // 打开相机
            checkPoint(status);                                                                      // check point 3
            status = GXSetAcqusitionBufferNumber(hDevice, 2);                                        // 设置缓存空间大小
            checkPoint(status);                                                                      // check point 4
            status = GXSetBool(hDevice, GX_BOOL_CHUNKMODE_ACTIVE, true);                             // 设置帧信息
            checkPoint(status);                                                                      // check point 5
            status = GXSetEnum(hDevice, GX_ENUM_CHUNK_SELECTOR, GX_CHUNK_SELECTOR_CHUNK_TIME_STAMP); // 启用时间戳
            checkPoint(status);                                                                      // check point 6
            GXStreamOn(hDevice);                                                                     // 开始读取视频流
            status = GXSetFloat(hDevice, GX_FLOAT_EXPOSURE_TIME, 4700);                              // 设置曝光时间
            checkPoint(status);                                                                      // check point 7
            status = GXSetEnum(hDevice, GX_ENUM_GAIN_SELECTOR, GX_GAIN_SELECTOR_ALL);                // 选择全通道增益
            checkPoint(status);                                                                      // check point 8
            status = GXSetFloat(hDevice, GX_FLOAT_GAIN, 16);                                         // 设置增益值为16
            checkPoint(status);                                                                      // check point 9
            auto qos = rmw_qos_profile_default;                                                      // 设置消息参数
            camera_pub_ = image_transport::create_camera_publisher(this, "image_raw", qos);
            camera_name_ = this->declare_parameter("camera_name", "daheng_camera");
            camera_info_manager_ =
                std::make_unique<camera_info_manager::CameraInfoManager>(this, camera_name_);
            auto camera_info_url = this->declare_parameter(
                "camera_info_url", "package://daheng_camera/config/camera_info.yaml");
            if (camera_info_manager_->validateURL(camera_info_url))
            {
                camera_info_manager_->loadCameraInfo(camera_info_url);
                camera_info_msg_ = camera_info_manager_->getCameraInfo();
            }
            else
            {
                RCLCPP_WARN(this->get_logger(), "Invalid camera info URL: %s", camera_info_url.c_str());
            }
            RCLCPP_INFO(this->get_logger(), "Publishing image!");
            image_msg_.header.frame_id = "camera_optical_frame";
            image_msg_.encoding = "rgb8";
            // bool a=false;
            while (true)
            {
                status = GXDQBuf(hDevice, &pFrameBuffer, 1000); // 调 用 GXDQBuf 取 一 帧 图 像
                checkPoint(status);                             // check point 10
                // status = GXGetInt(hDevice, GX_INT_PAYLOAD_SIZE, &g_nPayloadSize);
                image_msg_.data.reserve(pFrameBuffer->nWidth * pFrameBuffer->nHeight * 3);
                DX_BAYER_CONVERT_TYPE cvtype = RAW2RGB_NEIGHBOUR3; // 选 择 插 值 算 法
                DX_PIXEL_COLOR_FILTER nBayerType = DX_PIXEL_COLOR_FILTER(BAYERBG);
                bool bFlip = false;
                char *pRGB24Buf = new char[pFrameBuffer->nWidth * pFrameBuffer->nHeight * 3]; // 输 出 图 像 RGB 数 据
                VxInt32 DxStatus = DxRaw8toRGB24(pFrameBuffer->pImgBuf, pRGB24Buf, pFrameBuffer->nWidth, pFrameBuffer->nHeight, cvtype, nBayerType, bFlip);
                if (DxStatus != DX_OK)
                {
                    RCLCPP_ERROR(this->get_logger(), "Raw8 to RGB24 failed!");
                    if (pRGB24Buf != NULL)
                    {
                        delete[] pRGB24Buf;
                        pRGB24Buf = NULL;
                    }
                    return;
                }
                cv::Mat Src = cv::Mat(pFrameBuffer->nHeight, pFrameBuffer->nWidth, CV_8UC3);
                memcpy(Src.data, pRGB24Buf, pFrameBuffer->nWidth * pFrameBuffer->nHeight * 3);
                if (status == GX_STATUS_SUCCESS)
                {
                    cv::resize(Src, Src, cv::Size(640, 480));

                    image_msg_.data.assign(Src.datastart, Src.dataend);
                    camera_info_msg_.header.stamp = image_msg_.header.stamp = this->now();
                    image_msg_.height = Src.rows;
                    image_msg_.width = Src.cols;
                    image_msg_.step = Src.cols * 3;
                    image_msg_.data.resize(Src.rows * Src.cols * 3);
                    camera_pub_.publish(image_msg_, camera_info_msg_);
                    fail_conut_ = 0;
                }
                else
                {
                    RCLCPP_WARN(this->get_logger(), "Failed to get image buffer, status = %d", status);
                    fail_conut_++;
                }
                delete[] pRGB24Buf;
                pRGB24Buf = NULL;
                // //调 用 GXQBuf 将 图 像 buf 放 回 库 中 继 续 采 图
                status = GXQBuf(hDevice, pFrameBuffer);
                if (fail_conut_ > 5)
                {
                    RCLCPP_FATAL(this->get_logger(), "Failed to get image buffer, exit!");
                    rclcpp::shutdown();
                    break;
                }
            }
        }
    };
} // namespace daheng_camera
#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(daheng_camera::DaHengCameraNode)